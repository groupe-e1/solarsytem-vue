import Vue from 'vue'
import Router from 'vue-router'
import SolarSystemMainMenu from '@/components/SolarSystemMainMenu.vue'
import PlanetDetail from '@/components/PlanetDetail.vue'


Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: 'SolarSystemMainMenu', component: SolarSystemMainMenu },
    { path: '/planet/:id', name: 'PlanetDetail', component: PlanetDetail }
  ],
  mode: "history"
})